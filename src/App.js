import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;

/*
import React from 'react';
import logo from './logo.svg';
import './App.css';

import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
// import App from './App';
import Forms from './Forms';
import Login from './Login';
import Projects from './Projects';
import AddProject from './AddProject';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>

      <Router>
        <div>
          <switch>
            <Route exact path='/' component={App} />
            <Route path='/forms' component={Forms} />
            <Route path='/login' component={Login} />
            <Route path='/projects' component={Projects} />
            <Route path='/addproject' component={AddProject} />
          </switch>
        </div>
    </Router>
    
    </div>
  );
}

export default App;

*/
