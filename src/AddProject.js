import React, { Component } from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {Redirect} from 'react-router-dom';

class AddProject extends Component {
    constructor(props){
        let users = JSON.parse(localStorage.getItem('users_data'));
        super(props);
        this.state={
            id: 1,
            title: '',
            discription: '',
            t_lead: '',
            members: [],
            stUsers: users,
            project_count: JSON.parse(localStorage.getItem('projects_data')).length+1,
            projects: JSON.parse(localStorage.getItem('projects_data')),
            redirect: false
        }

        this.titleHandler = this.titleHandler.bind(this);
        this.discriptionHandler = this.discriptionHandler.bind(this);
        this.t_leadHandler = this.t_leadHandler.bind(this);
        this.membersHandler = this.membersHandler.bind(this);
        this.add_project = this.add_project.bind(this);
    }

    titleHandler(event){
        this.setState({
            title: event.target.value
        });
    }

    discriptionHandler(event){
        this.setState({
            discription: event.target.value
        });
    }

    t_leadHandler(event){
        this.setState({
            t_lead: event.target.value
        });
        console.log(this.state.t_lead);
    }

    membersHandler(event){
        console.log("val ====>>> ", event)
        this.setState({
            members: event.target.value
        });

        // let users = JSON.parse(localStorage.getItem('users_data'));

        // const opt = event.target.value;
        // const opt_values = [];

        // console.log(this.state.members);
        
        // for (let index = 0; index < opt_values.length; index++) {
        //     if(opt[index].selected){
        //         opt_values.push(users[opt[index].value-1]);
        //     }
        // }
        // // return opt_values
        // this.setState({
        //     members: opt_values
        // });
    }

    add_project(){
        // const projects_arr = [];
        const projects_arr = JSON.parse(localStorage.getItem('projects_data'));
        const u_arr = [];
        // const membersVal = members_arr
        // const u = JSON.parse(localStorage.getItem('users_data'));
            // console.log("state: "+this.state.members[2] +"<----> stored dara: "+this.state.stUsers[2].id)
            console.log("jksdfhshdf ", this.state.stUsers[0])
        for(let i=0; i<=this.state.stUsers.length-1; i++){
            // console.log("jksdfhshdf ", this.state.stUsers[i])
            console.log("i in for loop: ",i);
            for(let j=0; j<this.state.members.length; j++){
                if(this.state.stUsers[i].id === this.state.members[j]){
                    u_arr.push(this.state.stUsers[i]);
                    console.log("i in if condition: ",i);
                }
            }
        }
        console.log("array of users: ",u_arr);

        const project_obj = {
            id: this.state.project_count,
            title: this.state.title,
            discription: this.state.discription,
            t_lead: [this.state.stUsers[this.state.t_lead-1]],
            members: u_arr,
            task: []
        }
        console.log("dlfajfa",project_obj.members);
       if(project_obj.title !== '' && project_obj.discription !== '' && project_obj.t_lead !== ''  && project_obj.members !== ''){
        projects_arr.push(project_obj);
        localStorage.setItem('projects_data', JSON.stringify(projects_arr));
        // console.log(this.state.t_lead + " " + this.state.members);
        this.setRedirect();
       } else {
           console.log('Please insert all input values.');
       }
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        });
    }

    renderRedirectProjects = () => {
        if(this.state.redirect){
            return <Redirect to='/projects' />
        }
    }

    render(){
        return (
            <div>
                <h1>Add Project</h1>
                Title: <input type='text' name='title' onChange={this.titleHandler} />
                Discription: <textarea name='discription' onChange={this.discriptionHandler} />
                Team Lead: <select name='teamLead' value={this.state.t_lead} onChange={this.t_leadHandler}>
                {this.state.stUsers.map((user) => <option key={user.id} value={user.id}>
                    {user.f_name}
                </option>)}
                </select>
                {/* Members: <select name='members' onChange={this.membersHandler} multiple size="5">
                    {this.state.stUsers.map((u) => <option key={u.id}>
                    {u.f_name}
                </option>)}
                </select> */}
Members:<Select
                labelId="demo-mutiple-name-label"
                id="demo-mutiple-name"
                multiple
                value={this.state.members}
                onChange={this.membersHandler}
                >
                {this.state.stUsers.map(name => (
                    <MenuItem key={name.id} value={name.id}>
                        {name.f_name}
                    </MenuItem>
                ))}
        </Select>
                <button onClick={this.add_project} name='submit'>Submit</button>
            </div>
        );
    }
}

export default AddProject;