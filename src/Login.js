import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';

class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            redirect: false
        }
        this.emailHandler = this.emailHandler.bind(this);
        this.passwordHandler = this.passwordHandler.bind(this);
        this.loginHandler = this.loginHandler.bind(this);
        // this.renderRedirectProjects = this.renderRedirectProjects.bind(this);
    }

    emailHandler(event){
        this.setState({
            email: event.target.value
        });
    }

    passwordHandler(event){
        this.setState({
            password: event.target.value
        });
    }

    loginHandler(){
        //Fetching users from localstorage
        let usersData = JSON.parse(localStorage.getItem('users_data'));
        for (let index = 0; index < usersData.length; index++) {
            if(usersData[index].email === this.state.email && usersData[index].password === this.state.password){
                console.log('You are successfully loged in.');
                this.setRedirect();
                // console.log("redirect is: "+this.redirect);
            }
        }
    }
    
    setRedirect = () => {
        this.setState({
            redirect: true
        });
    }

    renderRedirectProjects = () => {
        if(this.state.redirect){
            return <Redirect to='/projects' />
        }
    }


    render(){
        return(
            <div>
                <h1>Login</h1>
                Email: <input type='text' name='email' value={this.state.email} onChange={this.emailHandler}/>
                Password: <input type='text' name='password' value={this.state.password} onChange={this.passwordHandler} />
                {this.renderRedirectProjects()}
                <button onClick={this.loginHandler}>Login</button>
                <p>{this.state.email}{this.state.password}</p>
            </div>
        );
    }
}

export default Login;