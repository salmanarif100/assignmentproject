import React, { Component } from 'react';
// import Login from './Login';
// import ReactDOM from 'react-dom';
// import { Link} from 'react-router-dom'
// import { Link, withRouter } from "react-router-dom";
import {Redirect} from 'react-router-dom';

class Forms extends Component {
    constructor(props){
        const users = JSON.parse(localStorage.getItem('users_data'));
        super(props);
        this.state = {
            id: users.length+1,
            f_name: '',
            l_name: '',
            designation: '',
            email: '',
            password: '',
            redirect: false,
            users: []
        }
        this.f_nameHandler = this.f_nameHandler.bind(this);
        this.l_nameHandler = this.l_nameHandler.bind(this);
        this.designationHandler = this.designationHandler.bind(this);
        this.emailHandler = this.emailHandler.bind(this);
        this.passwordHandler = this.passwordHandler.bind(this);
        this.clickHandler = this.clickHandler.bind(this);
        this.firstUser = this.firstUser.bind(this);
    }

    f_nameHandler(event){
        this.setState({
            f_name: event.target.value
        });
    }

    l_nameHandler(event){
        this.setState({
            l_name: event.target.value
        });
    }

    designationHandler(event){
        this.setState({
            designation: event.target.value
        });
    }

    emailHandler(event){
        this.setState({
            email: event.target.value
        });
    }

    passwordHandler(event){
        this.setState({
            password: event.target.value
        });
    }

    clickHandler(){
        // let currentUser = this.state.users;
        // let currentUser = [];

        const user_obj = {
            id: this.state.id,
            f_name: this.state.f_name,
            l_name: this.state.l_name,
            designation: this.state.designation,
            email: this.state.email,
            password: this.state.password
        }

        // currentUser.push(user_obj);
        

        if(user_obj.f_name === '' || user_obj.l_name === '' || user_obj.designation === '' || user_obj.email === '' || user_obj.password === ''){
            console.log('Please fill complete form.')
        }else if(!this.state.email.includes('@') && !this.state.email.includes('.com')){
            console.log('invalid email');
        }else{
            this.setUsersData(user_obj);
            this.setRedirect();
        }
       
    }

    firstUser(){
        let currentUser = [];
        const user1_obj = {
            id: 1,
            f_name: this.state.f_name,
            l_name: this.state.l_name,
            designation: this.state.designation,
            email: this.state.email,
            password: this.state.password
        }
        currentUser.push(user1_obj);
        localStorage.setItem('users_data', JSON.stringify(currentUser));
    }

    // nextComponent(event){
    //     event.props.history.push("/login");
    // }

    setRedirect = () => {
        this.setState({
            redirect: true
        });
    }

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/login' />
        }
      }

    setUsersData(obj){
        let set_users = JSON.parse(localStorage.getItem('users_data'));
        set_users.push(obj);
        localStorage.setItem('users_data', JSON.stringify(set_users));
        // localStorage.removeItem('users_data');
    }

    render(){
        return(
            <div>
                <h1>Registration Form</h1>
                <div>
                    First Name: <input type='text' name='f_name' value={this.state.f_name } onChange={this.f_nameHandler} />
                </div>
                <div>
                    Last Name: <input type='text' name='l_name' value={this.state.l_name } onChange={this.l_nameHandler} />
                </div>
                <div>
                    Designation: 
                    <div className="dd-wrapper">
  <div className="dd-header">
    <div className="dd-header-title"></div>
  </div>
  <select className="dd-list" name='designation' value={this.state.designation} onChange={this.designationHandler}>
    <option className="dd-list-item">Admin</option>
    <option className="dd-list-item">Team Lead</option>
    <option className="dd-list-item">Senior Software Engineer</option>
    <option className="dd-list-item">Software Engineer</option>
    <option className="dd-list-item">junior Software Engineer</option>
  </select>
</div>
                </div>
                <div>
                Email: <input type='text' name='email' value={this.state.email } onChange={this.emailHandler} />
                </div>
                <div>
                Passowrd: <input type='text' name='password' value={this.state.password } onChange={this.passwordHandler} />
                </div>
                <div>
                    {this.renderRedirect()}
                    <button onClick={this.clickHandler}>Submit</button>
                    {/* <button onClick={this.setRedirect}>nextComponent</button> */}
                    {/* <Link to="/login">Next Component</Link> */}
                </div>
                {/* <p>{this.state.f_name}</p> */}
            </div>
            
        )
    }

}

export default Forms;