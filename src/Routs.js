import React from 'react';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom';
import App from './App';
import Forms from './Forms';
import Login from './Login';
import Projects from './Projects';
import AddProject from './AddProject';
import Task from './Task';
import AddTask from './AddTask'

const CustomeRoutes = () => (
    <Router>
        <div>
            <Route exact path='/' component={App} />
            <Route path='/forms' component={Forms} />
            <Route path='/login' component={Login} />
            <Route path='/projects' component={Projects} />
            <Route path='/addproject' component={AddProject} />
            <Route path='/task' component={Task} />
            <Route path='/addtask' component={AddTask} />
        </div>
    </Router>
);

export default CustomeRoutes;