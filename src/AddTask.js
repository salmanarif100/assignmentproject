import React, { Component } from 'react';
// import Select from '@material-ui/core/Select';
// import MenuItem from '@material-ui/core/MenuItem';
import {Redirect} from 'react-router-dom';

class AddTask extends Component {
    constructor(props){
        let users = JSON.parse(localStorage.getItem('users_data'));
        super(props);
        this.state={
            // task_id: JSON.parse(localStorage.getItem('tasks_data')).length+1,
            title: '',
            hours: '',
            date: '',
            discription: '',
            assignee: '',
            status: '',
            stUsers: users,
            project_id: JSON.parse(localStorage.getItem('project_id')),
            projects: JSON.parse(localStorage.getItem('projects_data')),
            redirect: false,
        }

        this.titleHandler = this.titleHandler.bind(this);
        this.hoursHandler = this.hoursHandler.bind(this);
        this.dateHandler = this.dateHandler.bind(this);
        this.discriptionHandler = this.discriptionHandler.bind(this);
        this.assigneeHandler = this.assigneeHandler.bind(this);
        this.statusHandler = this.statusHandler.bind(this);
        this.add_task = this.add_task.bind(this);
    }

    titleHandler(event){
        this.setState({
            title: event.target.value
        });
    }

    hoursHandler(event){
        this.setState({
            hours: event.target.value
        })
    }

    dateHandler(event){
        this.setState({
            date: event.target.value
        })
    }

    discriptionHandler(event){
        this.setState({
            discription: event.target.value
        });
    }

    assigneeHandler(event){
        this.setState({
            assignee: event.target.value
        })
        // console.log(this.state.assignee);
    }

    statusHandler(event){
        this.setState({
            status: event.target.value
        })
    }


    add_task(){
        const tasks_arr = [];
        // const tasks_arr = JSON.parse(localStorage.getItem('tasks_data'));
        // const u_arr = [];
        let task_id = 0;
        if(this.state.projects[this.state.project_id-1].task.length > 0){
            task_id = this.state.projects[this.state.project_id-1].task.length+1;
            tasks_arr.push(this.state.projects[this.state.project_id-1].task);
        } else{
            task_id = 1;
        }

        const task_obj = {
            id: task_id,
            // id: this.state.task_id,
            title: this.state.title,
            hours: this.state.hours,
            date: this.state.date,
            discription: this.state.discription,
            assignee: [this.state.stUsers[this.state.assignee-1]],
            status: this.state.status
        }
        console.log("dlfajfa",task_obj.members);
        if(task_obj.id !== '' && task_obj.title !== '' && task_obj.hours !== '' && task_obj.discription !== '' && task_obj.assignee !== '' && task_obj.status !== ''){
            tasks_arr.push(task_obj);
            // localStorage.setItem('tasks_data', JSON.stringify(tasks_arr));
            this.state.projects[this.state.project_id-1].task.push(task_obj);
            localStorage.setItem('projects_data', JSON.stringify(this.state.projects));
        } else{
            console.log("Please insert all input values.");
        }
        this.setRedirect();
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        });
    }

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/task' />
        }
      }

    render(){
        return (
            <div>
                <h1>Add Task</h1>
                Title: <input type='text' name='title' onChange={this.titleHandler} required/>
                Hours: <input type='number' name='hours' onChange={this.hoursHandler} required/>
                Date: <input type='date' name='date' onChange={this.dateHandler} required/>
                Discription: <textarea name='discription' onChange={this.discriptionHandler} required/>
                Assigned To: <select name='assignee' value={this.state.assignee} onChange={this.assigneeHandler} required>
                {this.state.stUsers.map((user) => <option key={user.id} value={user.id}>
                    {user.f_name}
                </option>)}
                </select>
                Status: <select name='status' value={this.state.status} onChange={this.statusHandler} required>
                    <option>New</option>
                    <option>In Progress</option>
                    <option>Done</option>
                    <option>Backlog</option>
                </select>

                <button onClick={this.add_task} name='submit' href='/task'>Submit</button>
            </div>
        );
    }
}

export default AddTask;