import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});


//Fetching projects from localStorage
const my_projects = JSON.parse(localStorage.getItem('projects_data'));

export default function SimpleTable() {
  const classes = useStyles();

  return (
<div>
    <AppBar position="relative">
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>
            Projects
          </Typography>
        </Toolbar>
    </AppBar>

    {/* Buttons */}
    <div className={classes.heroContent}>
          <Container maxWidth="sm">

            <div className={classes.heroButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button variant="outlined" color="primary" href="/login">
                    Back
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="contained" color="primary" href="/addproject">
                    Add Project
                  </Button>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>

<TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align="left">Title</TableCell>
            <TableCell align="left">Description</TableCell>
            <TableCell align="left">Team Lead</TableCell>
            <TableCell align="left">Members</TableCell>
            <TableCell align="left">Show Task</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {my_projects.map(p => (
            <TableRow key={p.id} value={p.id} href='/task'>
              <TableCell component="th" scope="row">
                {p.id}
              </TableCell>
              <TableCell align="left">{p.title}</TableCell>
              <TableCell align="left">{p.discription}</TableCell>
              <TableCell align="left">{p.t_lead.map(teamLead => (teamLead.f_name))}</TableCell>
              <TableCell align="left">{p.members.map(m => (m.f_name+', '))}</TableCell>
              <TableCell align="left">
                <Button onClick={() => { localStorage.setItem('project_id', p.id); }} key={p.id} variant="contained" color="primary" href="/task">
                  Details
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
</div>
  );
}


/************My own simple project page:**************
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class Projects extends Component{
    state = {
        redirect: false
    }

    setRedirect = () =>{
        this.setState({
            redirect: true
        });
        // goToAddProject();
    }

    goToAddProject = () => {
        if(this.state.redirect){
            return <Redirect to='/addproject' />
        }
    }

    render(){
        return(
            <div>
                <h1>Projects</h1>
                {this.goToAddProject()}
                <button onClick={this.setRedirect}>Add Project</button>
            </div>
        );
    }
}

export default Projects;
*/