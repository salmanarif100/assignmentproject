import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
// import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
// import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
// import Link from '@material-ui/core/Link';

// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="https://material-ui.com/">
//         Your Website
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

function ProjectName (){
  const projectName = JSON.parse(localStorage.getItem('projects_data'));
  return projectName[JSON.parse(localStorage.getItem('project_id')-1)].title;
}

// function TaskName (){
  const project_data = JSON.parse(localStorage.getItem('projects_data'));
  // return projectName[JSON.parse(localStorage.getItem('project_id')-1)].task[0].title;

  let project_details = {};
  let details_arr = [];
  let cards = [];

  if(project_data[JSON.parse(localStorage.getItem('project_id')-1)].task.length > 0){
    for(let i = 0; i < project_data[JSON.parse(localStorage.getItem('project_id')-1)].task.length; i++){
      // if(project_data[JSON.parse(localStorage.getItem('project_id')-1)].task.length > 0){
        project_details = {
          // project_title: project_data[JSON.parse(localStorage.getItem('project_id')-1)].title,
          task_title: project_data[JSON.parse(localStorage.getItem('project_id')-1)].task[i].status,
          task_description: project_data[JSON.parse(localStorage.getItem('project_id')-1)].task[i].discription,
          task_assignee: project_data[JSON.parse(localStorage.getItem('project_id')-1)].task[i].assignee[0].f_name,
        }
        details_arr.push(project_details)
        cards.push(i);
    }
  }
  console.log(details_arr);

const useStyles = makeStyles(theme => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));



export default function Album() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>
            {ProjectName()}
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <div className={classes.heroButtons}>
              <Grid container spacing={2} justify="center">
              <Grid item>
                  <Button variant="outlined" color="primary" href="/projects">
                    Back
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="contained" color="primary" href="/addtask">
                    Add Task
                  </Button>
                </Grid>

              </Grid>
            </div>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
              {details_arr.map(task => (
              <Grid item xs={12} sm={6} md={4}>
               
                <Card  key={task.id} className={classes.card}>
                  <CardContent className={classes.cardContent} >
                  <Typography gutterBottom variant="h5" component="h2">
                    {task.task_title}
                  </Typography>
                  <Typography>
                    {task.task_description}
                  </Typography>
                  <Typography gutterBottom variant="h7" component="h7">
                    {task.task_assignee}
                  </Typography>

                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          Footer
        </Typography>
        <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          This is my React Project and ALHMADULLILLAH I have done!
        </Typography>
        {/* <Copyright /> */}
      </footer>
      {/* End footer */}
    </React.Fragment>
  );
}